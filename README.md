# PHP OOP EXERCISE two!

_This project is a website where people write your name, age and gender and this basically validate this data. 

## Installation and initialization 🔧

_If you have already cloned the project code, you must install:_

```
XAMPP 
```

_To start the project in developer mode use the script:_

```
COMPOSER INSTALL
```

## Built with 🛠️

- [PHP](https://www.php.net/)
- [COMPOSER](https://getcomposer.org/)
- [HTML5](https://developer.mozilla.org/en-US/docs/Web/Guide/HTML/HTML5)
- [SASS](https://sass-lang.com/) 


## Authors ✒️

- **Lina Castro** - _Frontend and backend Developer_ - [lirrums](https://gitlab.com/linacastrodev)

## License 📄

This project is under the License (MIT)

## Acknowledgements 🎁

- We greatly appreciate to the company for motivating us with the project. providing us with tools and their knowledge for our professional growth📢
