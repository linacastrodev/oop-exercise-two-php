<!DOCTYPE html>
<html>
  <head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="src/assets/index.css" />
  </head>
  <body>
    <div class="container">
      <h1 class="title">OOP PHP EXERCISE!</h1>
      <p class="subtitle">Hey, welcome this little application for validate: <br> your age and gender!</p>
      <form method="POST" action="">
        <input type="text" class="form-contrl" placeholder="Enter your name" name="getname"/>
        <input type="number" class="form-contrl" placeholder="Enter your age" name="age"/>
        <select class="form-contrl" name="select">
          <?php $array = array(1 => 'Female', 2 => 'Male'); foreach ($array as $key => $value) {?>
          <option value="<?php echo $value; ?>"><?php echo $value; ?></option>
          <?php }?>
        </select>
        <input type="submit" class="btn-contrl" value="Submit" />
      </form>
    </div>
  </body>
</html>
<?php
require __DIR__ . '/vendor/autoload.php';
$valueAge = $_POST['age'];
$valueName = $_POST['getname'];
$valueGender = $_POST['select'];
if (isset($valueAge)) {
    echo '<span class="response-age">' .checkName($valueName). '</span>';
    echo '<span class="response-age">' .checkAge($valueAge). '</span>';
    echo '<br>';
    echo '<span class="response-gender">' . checkGender($valueGender) . '</span>';
}
?>