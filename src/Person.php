<?php
/**
 * Author: Lirrums
 * Programming Language: PHP
 * Topic: PHP + OOP + HTML + SASS
 * Gitlab: https://gitlab.com/linacastrodev
 */
namespace app;
class Person
{
    public $name;
    public $age;
    public $gender;

    public function __construct($name, $age, $gender)
    {
        $this->$name = $name;
        $this->$age = $age;
        $this->$gender = $gender;
    }
    public function getName($name)
    {
        return $name;
    }
    public function isAdult($age)
    {
        if (isset($age)) {
            if ($age >= 18) {
                return "You are a adult with age" . " " . $age . " welcome!";
            } else {
                return "You are a minor with age" . " " . $age . " you need into with a adult!";
            }
        } else {
            echo "Write some";
        }
    }
    public function yourGender($gender)
    {
        if (isset($gender)) {
            if ($gender == "Female") {
                return "You're a " . $gender;
            } else if ($gender == "Male") {
                return "You're a " . $gender;
            }
        } else {
            echo "Write some";
        }
    }
}
